# psd2-api-arz

Fixed psd2-api-1.3.4_arz.yaml to support maven-plugin-openapi-generator jaxrs-spec

(https://github.com/OpenAPITools/openapi-generator/issues/5565)